﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace TextWatermarker {
	internal static class Program {
		private static void Main() {
			while (true) {
				Console.Clear();
				string input;
				if (File.Exists("input.txt")) {
					input = File.ReadAllText("input.txt");
				} else {
					Console.WriteLine("Input source text:");
					input = Console.ReadLine();
				}

				Console.WriteLine("Choose option:\r\n1. Watermark text\r\n2. Unwatermark text\r\n3. Show all invisible symbols\r\n4. Exit");
				string option = Console.ReadLine();
				string result = "";
				Console.Clear();
				switch (option) {
					case "1":
						Console.WriteLine("Enter a watermark:");
						string watermark = Console.ReadLine();
						if (string.IsNullOrEmpty(watermark)) {
							Console.WriteLine("Input is incorrect");
							continue;
						}

						result = Watermark(input, watermark);
						break;
					case "2":
						result = Unwatermark(input);
						break;
					case "3":
						result = ShowSymbols(input);
						break;
					case "4":
						return;
					default:
						Console.WriteLine("Incorrect option");
						Thread.Sleep(500);
						break;
				}

				Console.WriteLine("Writing output to output.txt...");
				File.WriteAllText("output.txt", result);
			}
		}

		private static string Watermark(string input, string watermark) {
			Regex regex = new Regex(@"[^\u0020-\u007F]", RegexOptions.Compiled); // removing non-ASCII symbols
			watermark = regex.Replace(watermark, "");

			// Forming watermark using unicode tag symbols
			StringBuilder watermarkBuilder = new StringBuilder();
			foreach (char symbol in watermark) {
				watermarkBuilder.Append('\uDB40');
				watermarkBuilder.Append((char) (symbol + 56320));
			}

			string resultWatermark = watermarkBuilder.ToString();

			// Forming a new string with watermark after every letter
			StringBuilder result = new StringBuilder();
			foreach (char symbol in input) {
				if (!char.IsWhiteSpace(symbol) && !char.IsControl(symbol)) {
					result.Append(resultWatermark);
				}

				result.Append(symbol);
			}

			return result.ToString();
		}

		private static string Unwatermark(string input) {
			// Just replacing using regex, this removes all the tag symbols
			Regex regex = new Regex(@"[\uDC00-\uDC80\uDB40]", RegexOptions.Compiled);
			return regex.Replace(input, "");
		}

		private static string ShowSymbols(string input) {
			// Removing tag symbols suffix
			input = input.Replace("\uDB40", "");

			StringBuilder result = new StringBuilder();
			foreach (char symbol in input) {
				// Decoding tag symbols
				result.Append((symbol >= 56352) && (symbol <= 56447) ? (char) (symbol - 56320) : symbol);
			}

			return result.ToString();
		}
	}
}
